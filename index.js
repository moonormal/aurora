const mysql = require('mysql');
const express = require('express');
var app = express();
const bodyparser = require('body-parser');

const con = mysql.createConnection({
    host: "database-1-instance-1.csbntmfwjg9w.us-east-1.rds.amazonaws.com",
    user: "admin",
    password: "administrador",
    database: "main"
});

con.connect(function(err) {

    if (!err){
      console.log('Conectado a la base de datos\n');
      con.query('CREATE DATABASE IF NOT EXISTS main;');
      con.query('USE main;');
      con.query('CREATE TABLE IF NOT EXISTS users(id int NOT NULL AUTO_INCREMENT, username varchar(30), email varchar(255), age int, PRIMARY KEY(id));', function(error, result, fields) {
          console.log(result);
      });
    }else{
      console.log('Conexion fallida \n Error: '+ JSON.stringfy(err, undefined, 2));
    //con.end();
   }
});

//connection.end();

app.listen(3000,()=>console.log('Express server is running at port no: 3000'));

app.get('/users',(req,res)=>{
  con.query('SELECT * FROM users',(err, rows, fields)=>{
    if (err) {
      console.log('error1 '+err);
      return;
    }else{
      console.log(rows);
    }
  })

});

app.post('/users',(req,res)=>{
  let user = req.body;

  var sql = "SET @id = ?;SET @username = ?;SET @email = ?;SET @age = ?; \
  CALL usersAddOrEdit(@id,@username,@email,@age);";
  con.query(sql, [user.id, user.username, user.email, user.age], (err, rows, fields)=>{
    if (!err)
      rows.forEach(element => {
      if(element.constructor == Array)
        res.send('Nuevo usuario con id: : '+ element[0].id);
      });
    else
      console.log('error2 '+err);
  })

});
